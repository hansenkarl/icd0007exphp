<?php

$numbers = [1, 2, 5, 6, 2, 11, 2, 7];


function get_odd_numbers($list)
{
    $oddList = [];
    foreach ($list as $num) {
        if ($num % 2 === 1) {
            $oddList[] = $num;
        }
    }
    return $oddList;
}

function prettyPrint($list) {
    $formatted = "[" . $list[0];
    for ($i = 1; $i < sizeof($list); $i++) {
        $formatted = $formatted . ", " . $list[$i];
    }
    return $formatted . "]";
}

print prettyPrint(get_odd_numbers($numbers));