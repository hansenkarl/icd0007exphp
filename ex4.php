<?php

$numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15];

function fizzbuzz($list)
{
    foreach ($list as $num) {
        if ($num % 3 === 0 && $num % 5 === 0) {
            echo "fizzbuzz" . PHP_EOL;
        } else if ($num % 3 === 0) {
            echo "fizz" . PHP_EOL;
        } else if ($num % 5 === 0) {
            echo "buzz" . PHP_EOL;
        } else {
            echo $num . PHP_EOL;
        }
    }
}

fizzbuzz($numbers);