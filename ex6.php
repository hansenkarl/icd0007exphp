<?php

$file = 'data/grades.txt';

$lines = file($file);

$total = 0;

foreach($lines as $line) {
    $line = trim($line);
    $line = explode(";", $line);
    $total += $line[1];
}

echo $total / count($lines);