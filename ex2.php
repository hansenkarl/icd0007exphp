<?php
/*
 * Kirjutage funktsioon is_in_list($list, $element_to_be_found), mis
   ütleb kas listis on selline element või mitte.

   is_in_list([1, 2, 3], 2) tagastab true;
   is_in_list([1, 2, 3], 4) tagastab false;
 */

$numbers = [1, 2, '3', 6, 2, 3, 2, 3];

function is_in_list($list, $element_to_be_found) {
    foreach ($list as $num) {
        if ($num === $element_to_be_found) {
            return true;
        }
    }
    return false;
}

print is_in_list($numbers, 1) ? "true" : "false";