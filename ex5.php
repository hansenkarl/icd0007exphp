<?php

$numbers = [3, 2, 5, 6];

function mkStr($list)
{
    $res = $list[0];
    for ($i = 1; $i < sizeof($list); $i++) {
        $res = $res . ", " . $list[$i];
    }
    return $res;
}

function mkList($list)
{
    return explode(",", $list);
}

$temp = mkStr($numbers);
print_r($temp . PHP_EOL);
$temp = mkList($temp);
print_r($temp);